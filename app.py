from flask import Flask,render_template
import os

app = Flask(__name__)

@app.route('/script_test')
def script_test_via_js():
  return "salut c'est l'output d'une fonction via javascript"

@app.route('/')
def script_output():
    output="salut c'est l'output d'une function depuis un template"
    return render_template('hello.html',output=output)

if __name__ == '__main__':
    app.run(debug=True)
